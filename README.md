# FFXI Auto Fighter - Tyr

Tyr is an FFXI auto fighter script built as a Windower 4 addon

Capability:
- Pulls target.
- Weaponskills above 1000TP.
- Uses abilities.
- Moves to target (WIP).

### Getting Started
- Download the contents of this project into your Windower/Addons folder.
- Rename the downloaded folder to "Tyr".
- Load the addon in-game via the windower console: `lua load tyr`.

### FFXI Chat Commands
- `//tyr on` - Starts Tyr.
- `//tyr off` - Stops Tyr.
- `//tyr reload` - Updates changes to the settings.xml file.
