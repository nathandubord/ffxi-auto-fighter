_addon.name = 'tyr'
_addon.author = 'nathandubord'
_addon.version = '0.1'
_addon.commands = {'Tyr'}

require('chat')
require('logger')
require('tables')
CONFIG = require('config')
PACKETS = require('packets')
RESOURCES = require('resources')

-- Reference material:
-- Lua guide for progammers: https://forums.windower.net/index.php@sharelink=download%3BaHR0cDovL2ZvcnVtcy53aW5kb3dlci5uZXQvaW5kZXgucGhwPy90b3BpYy82OTctbHVhLWd1aWRlLWZvci1wcm9ncmFtbWVycy8,%3BTHVhIEd1aWRlIGZvciBwcm9ncmFtbWVycw,,.html

-- Helper functions:
-- log('Spell: ', settings.spell) - prints "Spell: spellName" to the ffxi console
-- table.print({1, 2, 3}) - prints objects to the ffxi console

-- States
IS_TYR_RUNNING = false
CHARACTER_DELAY = 0

-- Constants
ACTION_DELAY = 2

-- Default Settings, these specify the types for settings.xml
DEFAULTS = {
	weaponskill = '',
	targetMobs = '',
	pullingAction = '',
}

-- Loads settings.xml and adds any missing fields from the defaults object
SETTINGS = CONFIG.load(DEFAULTS)

-- Listens for commands from ffxi chatlog/console
windower.register_event('addon command', function (...)
	local commands	= T{...}:map(string.lower)

	if not commands[1] or commands[1] == "help" then
		windower.add_to_chat(2, "Invalid command")
	elseif commands[1] == "on" then
		windower.add_to_chat(2, "Tyr has started")
		IS_TYR_RUNNING = true
		Engine()
	elseif commands[1] == 'off' then
		windower.add_to_chat(2, "Tyr has ended")
		IS_TYR_RUNNING = false
	end
end)

windower.register_event('outgoing chunk', function(id, data)
    if id == 0x015 then
        local action_message = PACKETS.parse('outgoing', data)
		PlayerH = action_message["Rotation"]
	end
end)

function Engine()
	if CHARACTER_DELAY == 0 then
		Battle()
	else
		-- Since the coroutine is in seconds, when we deduct 1, we're essentially saying to wait a second.
		CHARACTER_DELAY = CHARACTER_DELAY - 1
	end
	if IS_TYR_RUNNING then
		-- Windower helper function, time in seconds.
		coroutine.schedule(Engine, 1)
	end
end

function Battle()
	if windower.ffxi.get_player().in_combat then
		Rotate()
		-- if CanUseWeaponskill() then
		--	Weaponskill()
		--elseif CanUseAbility() then
		--	UseAbility()
		--end
	else
		Pull()
	end
end

function CanUseWeaponskill()
	return windower.ffxi.get_player().vitals.tp > 1000 and windower.ffxi.get_mob_by_target('t').distance:sqrt() < 3.0
end

function Weaponskill()
	windower.send_command(SETTINGS.weaponskill)
	CHARACTER_DELAY = 2
end

function CanUseAbility(abilityName)
	local canUseAbility = false
	local ability = RESOURCES.job_abilities:with('name', abilityName)
	Recasts = windower.ffxi.get_ability_recasts()
	if Recasts[ability.recast_id] == 0 and CHARACTER_DELAY == 0 then
		canUseAbility = true
	end
	return canUseAbility
end

function UseAbility()
	for abilityName in SETTINGS.abilities do
		local ability = RESOURCES.job_abilities:with('name', abilityName)
		Recasts = windower.ffxi.get_ability_recasts()
		if (Recasts[ability.recast_id] == 0) and CHARACTER_DELAY == 0 then
			windower.send_command(abilityName)
			CHARACTER_DELAY = 1
			return
		end
	end
end

function Pull()
	local mobsInMemory = windower.ffxi.get_mob_array()
	local pullingAction = RESOURCES.job_abilities:with('name', SETTINGS.pullingAction)
	for key, mob in pairs(mobsInMemory) do
		-- If the mob is in the targetMobs list
		if SETTINGS.targetMobs == mob['name']:lower() then
			if mob.valid_target and mob["hpp"] == 100 then
				-- If the pulling ability can pull the mob
				if pullingAction.range > mob.distance:sqrt() and CanUseAbility(pullingAction.name) then
					local player = windower.ffxi.get_player()
					-- Target packet. Reference: https://github.com/Windower/Lua/issues/1273
					PACKETS.inject(PACKETS.new('incoming', 0x058, {
						['Player'] = player.id,
						['Target'] = mob.id,
						['Player Index'] = player.index,
					}))
					local pullingCommand = "input /ja " .. pullingAction.name .. " <t>";
					windower.send_command(pullingCommand)
					windower.send_command("input /attack <t>")
				end
			end
		end
	end
end

function Rotate()
	if windower.ffxi.get_mob_by_target('t') then
		local destX = windower.ffxi.get_mob_by_target('t').x
		local destY = windower.ffxi.get_mob_by_target('t').y
		local direction = math.abs(PlayerH - math.deg(HeadingTo(destX,destY)))
		if direction > 10 then
			windower.ffxi.turn(HeadingTo(destX,destY))
		end
	end
end

function HeadingTo(destX, destY)
	local x = destX - windower.ffxi.get_mob_by_id(windower.ffxi.get_player().id).x
	local y = destY - windower.ffxi.get_mob_by_id(windower.ffxi.get_player().id).y
	local h = math.atan2(x, y)
	return h - 1.5708
end
